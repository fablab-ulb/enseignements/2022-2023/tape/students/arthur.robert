# arthur.robert

## Présentation

Bonjour, je suis Arthur, 22 ans, étudiant en première année de Master en Physique à l'Université Libre de Bruxelles et j'ai à l'idée de me spécialiser en physique des systèmes complexes appliqué à la biophysique. J'ai décidé de suivre le cours de TAPE afin de me familiariser avec la physique expérimentale ainsi qu'avec l'interdisciplinarité, cette dernière étant au coeur de la biophysique.

## Introduction

Dans le cadre de la collaboration entre la Belgique et l'université de La Havane visant à surveiller la pollution des eaux de la baie de La Havane sous contraintes frugales, nous allons nous pencher en profondeur sur les aspects techniques du _Foldscope_. Ce microscope, dont le coût de fabrication avoisinne les 50 centimes, présente une structure en origami qui lui permet d'atteindre des précsions d'alignement de l'ordre du micromètre.

A vrai dire, l'intérêt porté au _Foldscope_ provient du fait qu'il nous faudrait un moyen de mesurer en continu la concentration en planctons des eaux de la baie et l'analyse au microscope est une façon immédiate de le faire. L'étude de la concentration en planctons de ces eaux est motivée par le fait qu'une eau fortement polluée se traduit par une concentration en nutriments excessive et donc une concentration en planctons qui s'y adapte. Une mesure de la concentration en planctons nous donne donc une bonne idée de la qualité de l'eau. Il nous est conséquemment naturel de considérer le _Foldscope_ à bord d'une bouée de contrôle des eaux frugale.

## Journal de bord

### 26 Septembre 2022 : Introduction au _Foldscope_ 

En premier lieu, nous avons dû identifier certains aspects "techniques" remarquables du _Foldscope_ que nous aurions aimé approfondir. Pour ce faire, il nous a été recommandé de lire en profondeur [l'article de Manu Prakash](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781) présentant le micoscope en question. 
A la suite de cette lecture, j'ai été profondément impressionné par les multiples prouesses techniques qu'il a fallu mettre en oeuvre pour réaliser cet outil. Le fait qu'un outil aussi peu onéreux puisse, certes avec un champ d'action plus ou moins restreint, être aussi performant qu'un microscope classique à plusieurs centaines d'euros me paraissait, et me parait toujours, surréaliste. C'est à se demander si les microscopes classiques ne deviendront pas bientôt obselète tant les coûts de fabrication et les prix du marché passent du simple au centuple. 

J'ai ci-dessous regroupé plusieurs questionnements sur les aspects techniques du _Foldscope_ que j'ai eu en parcourant l'[article](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781) qui le présente. Je les ai rassemblé en deux thèmes principaux :

- **La physique derrière les origamis.** 
Quelles sont les contraintes mécaniques apportées au papier lorsqu'on le soumet à un pliage (origami) ? Comment ces contraintes influent-elles sur la stabilité du dispositif optique ? Le pliage est-il un moyen fiable et reproductible d'imposer une certaine stabilité à notre outil ? Les outils présentants des plis dans leur structure sont-ils durables dans le temps ?

- **L'optimisation de l'optique.** 
Comment procéder afin d'éviter les abbérations optique dûes aux bords de la lentille sphérique ? Est-ce que le fait que la lentille sphérique soit de petit diamètre (et donc de gros grossissement) induit que l'on puisse avoir un dispositif optique aussi compact ? Dans quelle mesure est-il possible de combiner plusieurs techniques de microscopie dans un unique _Foldscope_ ? Quelles sont les contraintes que cela engendre ? 

Toutes ces questionnements sont légitimes car leurs réponses sont cruciales au succès du _Foldscope_.

Sur le sujet de la reproductibilité par exemple, comment peut-on assurer que deux personnes montant leur _Foldscope_ respectif vont finalement obtenir un outil dont la fiabilité est équivalente ? On pourrait en effet se dire que la moindre erreur dans l'axe d'un des plis peut entacher la fiabilité de notre microscope et donc résulter en un outil défectueux hors d'usage. Il se trouve que les pliages origamis imposent des contraintes mécaniques qui assurent la reproductibilité des plis et donc la bonnne intégrité du microscope. Ainsi, si l'un des plis est raté par exemple, alors les plis suivants ne seront pas faisables de sorte qu'il faille d'abord réparer l'erreur faite précédemment. Ce mécanisme essentiel à la production de masse et rapide du _Foldscope_ mérite que l'on s'y penche de plus près. J'ai ici essayé d'en apporter une explication qualitative à la situation mais une étude plus approfondie serait évidemment la bienvenue. 

Par ailleurs, l'optimisation du dispositif optique est aussi d'une importance capitale. En effet, les contraintes frugales sous lesquelles la fabrication du _Foldscope_ s'inscrit impose l'utilisation de lentilles sphériques, qui sont les lentilles les moins chères à produire à grande échelle. Partant de cette contrainte là, il faut alors réfléchir à la bonne implémentation de lentilles sphériques dans le _Foldscope_ pour rendre ce dernier le plus fiable possible. En fait, lorsque l'on travaille avec des lentilles sphériques, on fait face à un type d'aberrations appelé les _aberrations sphériques_. Ces dernières se traduisent en le fait que les rayons lumineux frappants les bords de la lentille sont plus réfractés qu'ils ne le "devraient" comparés aux rayons frappants les régions de la lentille plus proche de son centre. On obtient donc une situation dans laquelle les rayons ne sont pas tous concentrés en un même point focal et donc notre image n'est pas nette. Afin d'éviter celà, on essaye de stopper au maximum les rayons ayant frappé les bords de notre lentille avec un cache par exemple. Là aussi, une étude plus en profondeur serait la bienvenue. Ci-dessous se trouve un schéma comparatif de deux types de lentille : une lentille idéale sans aberrations sphériques (en haut), et une lentille sphérique réelle (en bas).

![spherical aberration](img/SPH.png)

D'autres aspects intéressants portants sur l'impact du _Foldscope_ en tant qu'instrument d'apprentissage et de sensibilisation ont aussi été relevés pendant le cours. 
Le _Foldscope_ est un outil qui s'introduit parfaitement dans une initiation aux disciplines scientifiques destinée à des enfants/adolescents de par le monde, que ce soit dans des pays en voie de développement ou non.  

Il peut aussi s'inscrire dans de larges campagnes de sensibilisation en matière d'hygiène, de normes sanitaires ou de santé publique. Le _Foldscope_ peut en effet très bien s'insérer dans le diagnsotic de bon nombre de maladies ainsi que dans le contrôle de pathogènes dans certaines zones à risque mais nous y reviendrons. 

Par la suite, nous avons eu un premier contact avec le microscope dans le sens ou nous avons du le monter à partir du kit de base. Cette étape m'a pris en tout et pour tout 20 minutes. Ceci montre encore une fois l'accessibilité d'accès que le _Foldscope_ représente. 

![Foldscope](img/foldscope.jpg)

### 03 Octobre 2022 : Etat de l'art des utilisations du _Foldscope_ et observations

Le but du cours était cette fois de trouver des applications concrètes du _Foldscope_ dans le monde. Ces applications peuvent être d'un ressort humanitaire, médical, sanitaire, académique, environnemental, etc. C'est assez impressionnant de voir à quel point cet outil s'est répandu sous diverses formes dans tant de projets. On retrouve une liste non-exhaustive des projets dans lesquels ce microscope frugal s'insère sur le [site internet du _Foldscope_](https://microcosmos.foldscope.com/). On voit très vite que cet instrument est très principalement utilisé dans les pays en voie de développement dont l'Inde est un acteur majeur. Il permet donc de mettre à l'oeuvre des projets de recherche / campagnes de sensibilisation qui sans lui n'auraient pas été possibles faute de moyens. Je vous présente ici trois articles traitants de sujets diverses et variés mais ayant comme point commun d'insérer l'utilisation du _Foldscope_ dans leur mode opératoire : 

- [**Les _Foldscopes_ comme outil abordable dans l'étude des cellules souches de la moelle osseuse**](https://www.researchgate.net/profile/Arun-Dev-Sharma/publication/338884371_Foldscope_and_its_Applications/links/5e310b5792851c7f7f08cc9f/Foldscope-and-its-Applications.pdf#page=55)

L'article ici entreprend d'étudier l'évolution sous certaines conditions d'une population d'un certain type de cellules souches à l'aide du _Foldscope_. Cette étude est motivée par le fait que les cellules souches représentent un intérêt grandissant dans le domaine des sciences médicales du fait de leur grande différentiabilité ainsi que de leur auto-renouvellement. Ces deux caractéristiques des cellules souches laissent à penser que ces dernières pourraient être à la base de futurs traitements thérapeutiques. 
On cherche ici à étudier le comportement d'une population de cellules souches de la moelle osseuse soumises à un environnement comportant du _delta methrin_, un pesticide très utilisé en agriculture. Pour ce faire, on expose les cellules à des concentrations plus ou moins élevées de pesticide pendant plus ou moins de temps et puis on observe au _Foldscope_ la fraction de population encore viable par simple comptage. Les cellules non-viables sont en fait aisément identifiables de par leur morphologie car elles traversent un processus cellulaire appelé _apoptose_ (processus de mort programmée) . On tire de ces expériences que la fraction de cellules endommagées par la présence de pesticides augmente avec le temps d'exposition et avec la concentration du _delta methrin_.

![image cellules souches](img/deltamethrinstemcells.png)

- [**Etude de l'embryogénèse du poisson-zèbre à l'aide du _Foldscope_**](https://www.researchgate.net/profile/Arun-Dev-Sharma/publication/338884371_Foldscope_and_its_Applications/links/5e310b5792851c7f7f08cc9f/Foldscope-and-its-Applications.pdf#page=67)

La biologie du développement est une discipline de la biologie dont la compréhension est vitale à l'étude d'autres sujets allant du processus de vieillissement à celui de la régénération, ou encore à l'apparition de cancers etc. Dans ce cadre d'étude, le poisson-zèbre est un cobaye parfait. Cycle de reproduction rapide, fertilisation externe et oeufs transparents à la lumière, toutes ces caractéristiques justifient notre attrait pour cet organisme en rendant son étude très facile. Ainsi, les auteurs de [l'article](https://www.researchgate.net/profile/Arun-Dev-Sharma/publication/338884371_Foldscope_and_its_Applications/links/5e310b5792851c7f7f08cc9f/Foldscope-and-its-Applications.pdf#page=67) se proposent de comparer le suivi du développement d'un embryon de poisson-zèbre avec un microscope classique et un _Foldscope_. En plaçant les embryons dans un environnement propice à leur développement, on les photographie à intervalles réguliers et on compare la qualité des images obtenues avec les deux dispostifs optiques différents. On en vient rapidement à la conclusion que le _Foldscope_ égale le microscope classique en terme de qualité d'images et peut donc prétendre à être un outil permettant le suivi de l'embryogénèse du poisson-zèbre et ce, à moindre coût. Notons tout de même que le _Foldscope_ utilisé a dû être légèrement modifié de telle sorte que l'on puisse insérer un embryon dans le porte-échantillon (ce dernier a dû être élargi).

![images embryons](img/embryos.jpg)

- [**Le _Foldscope_ comme outil pour dépister les infections parasitaires chez les animaux sauvages dans le Tamil Nadu**](https://www.researchgate.net/profile/Arun-Dev-Sharma/publication/338884371_Foldscope_and_its_Applications/links/5e310b5792851c7f7f08cc9f/Foldscope-and-its-Applications.pdf#page=135)

Le dépsitage d'infections parasitaires chez les animaux sauvages en captivité se révèle d'une importance capitale dans la prévention de transmission de ces infections vers l'Homme. Lorsque les animaux sauvages passent d'un état de liberté à un état de captivité, ils développent une vulnérabilité plus importante vis-à-vis des infections parasitaires et ce, malgré les soins et l'attention qui leur est porté. Ainsi, si l'on veut éviter une transmission à l'Homme trop importante de parasites dits _zoonotiques_, il est primordial de dépister le plus vite possible la présence de parasites au sein de l'orgnaismes de ces animaux en captivité car ces infections sont un véritable danger pour l'Homme comme pour les animaux.
Pour ce faire, l'équipe qui est à l'origine de ce papier s'est lancé comme défi d'essayer de dépister des infections parasitaires chez deux espèces d'animaux captifs à l'aide d'un _Foldscope_ et d'un microscope classique afin de comparer les résultats obtenus à l'aide des deux outils. Ici, les chercheurs ont étudié des prélèvements de fèces d'éléphants et d'ours lippu venants de plusieurs sanctuaires, zoos et temples de la région du Tamil Nadu. 
L'idée primaire est de séparer les oeufs de parasites du reste des fèces en utilisant diverses techniques de dissociation des différentes composantes d'un prélèvement par leur densité. En utilisant des techniques permettant une différentiation en densité de plus en plus raffinée, on arrive finalement à isoler les oeufs de parasites, s'il y en a évidemment. 
Les prélèvements sont tout d'abord soumis à une sendimentation par centrifugation dans le but d'effectuer un première séparation en densité. 
Ensuite, et dans le but de raffiner la séparation en densité, il est possible d'utiliser la technique de flottaison pour isoler les potentiels oeufs de parasites. 

![float](img/float.png)

Ici, l'usage d'une solution à densité bien choisie fera flotter tout "corps" moins dense que cette dernière. Ainsi, si l'on crée une solution tout juste plus dense que des oeufs de parasites, alors ces derniers flotteront (tout comme d'autres débris de densité inférieure à la solution) et nous pourrons alors plus facilement les identifier et les observer sous un microscope. 
A la suite de cette étude, les scientifiques y ayant contribué se sont rendus compte que les microscopes optiques classiques étaient tout de même plus performants que les _Foldscope_ dans le sens où plus d'oeufs de parasites avaient été identifié avec les microcopes classiques qu'avec les _Foldscopes_. Ceci est probablement du au fait que la taille des échantillons que l'on peut étudier au _Foldscope_ est restreinte par rapport à un microscope ordinaire. Ainsi, comme la surface scannée à chaque instant est plus petite, le nombre d'oeufs identifiables et plus petit lui aussi et donc l'outil est moins performant.

_OBSERVATION PERSONNELLE_

Afin d'approfondir notre compréhension du _Foldscope_, il nous a été proposé de faire une observation d'un spécimen de notre choix avec le microscope préalablement monté. J'ai ici décidé d'observer un insecte trouvé dans mon jardin. 
Lors de cette observation, quelques problèmes encore non résolus sont survenus :
- comment faire bon usage du calibrage x-y (je n'ai pas réussi à faire bouger l'échantillon en même temps que les deux "plateformes" amovibles)
- la mise au point en z est très sensible et ne peut pas se faire en temps réel (on doit à chaque fois cfaire varier la distance z, puis vérifier si la mise au point est satisfaisante)
- les sources de lumières sont compliquées à centrer

Je me suis alors rendu compte à quel point la découverte par soi-même de cet outil était technique au premier abord et nécessitait l'aide d'une personne avec une réelle expertise, ce que je n'envisageais pas avant. En effet, lorsque nous avons débriefé nos observations personnelles tous ensemble, j'ai très vite réussi à combler mes lacunes à l'aide des expériences des autres étudiants. 

Enfin, voici quelques clichés pris lors de l'observation :


![insect2](img/insect2.jpg)
![insect3](img/insect3.jpg)

Dans le cadre de cet exercice, j'ai finalement décidé de mesurer la longueur d'une partie de la patte de l'insecte que j'ai récolté dans mon jardin. Plus précisement, j'ai mesuré la partie du corps de cet insecte qui part du bout de sa patte et qui finit à la deuxième articulation. Pour cela, j'ai d'abord réalisé deux clichés :  un premier de la grille de mesure de cellules de 0.5mm x 0.5mm ainsi qu'un cliché de la partie de la patte à mesurer. 

![grid](img/grid.jpg)
![leg](img/leg.jpg)

J'ai alors utilisé le logiciel de traitement d'image GIMP afin de mesurer combien de pixels de longueur faisait une cellule de la grille de mesure afin de comparer cette valeur avec le nombre de pixels de longueur que faisait la partie de la patte de l'insecte que je désirais mesurer. 
J'ai alors obtenu que cette partie de la patte de l'insecte faisait à peu près 0.46 mm.

### 10 Octobre 2022 : Défi et exploration

Dans cette section, je me propose de trouver un moyen de rendre l’utilisation du _Foldscope_ la plus accessible et ergonomique possible. En effet, après avoir manié cet outil plusieurs heures durant, je me suis vite rendu compte que les contraintes frugales imposées à la création du _Foldscope_ avaient eu comme répercussion première d’entacher l’expérience utilisateur.Mettre l’accent sur l’expérience utilisateur est bien évidemment optionnel lorsque le _Foldscope_ est à usage pédagogique ou initiatique car les utilisateurs dont il est question ne le sont que pour quelques heures tout au plus. Néanmoins, pour des personnes dont c’est la profession de manier des microscopes frugaux (chercheurs, laborantins, agriculteurs, … voir utilisation du _Foldscope_), cette notion d’ ”expérience utilisateur” devient primordiale. 
Il y bon nombre d’aspects à améliorer au _Foldscope_ : stabilité du dispositif, éclairage, couplage de la lentille à la caméra du téléphone, etc. 
Partant de toutes ces constatations, je me propose donc d’effectuer un prototype en carton de ce à quoi pourrait ressembler un socle fixe dans lequel viendrait s’insérer le _Foldscope_. **Le but final de cette section étant de rendre l'utilsation du _Foldscope_ la plus ergonomique et stable possible.**

Mais avant de rentrer dans le vif du sujet, je décide de procéder à un état de l'art des différentes améliorations faites au _Foldscope_ de par la littérature scientifique. En consultant les articles présents sur le [site internet du _Foldscope_](https://foldscope.com/pages/publications), j'en repère quelques-uns qui me semblent intéressants dans leurs manières d'aborder la mise en usage de ce microscope frugal. 

- Premièrement, et cela parait être à première vue une amélioration évidente tant elle est simple, les auteurs de [ce papier](https://www.sumathipublications.com/index.php/ijcbr/article/view/219/264) portant sur la faisabilité ou non du comptage de cellules sanguines au _Foldscope_ ont tout simplement décidé de fixer le _Foldscope_ à un smartphone à l'aide du coupleur magnétique fourni dans le kit de base du microscope et ont stabilisé le smartphone avec un dispositif qui s'y prête assez bien (voir fig 1.(a)). Le même type de dispositif est utilisé [ici](https://sumathipublications.com/index.php/ijcbr/article/view/194/226) dans un article qui étudie cette fois la faisabilité ou non de l'utilisation du _Foldscope_ en histopathologie clinique. On voit au figure 4 et 5 que les smartphones sont cette fois montés sur des trépieds.

- Deuxièmement, une autre amélioration ayant été envisagée dans la littérature est la finesse de la mise au point des microscopes origamis. Par exemple, [cet article](https://onlinelibrary.wiley.com/doi/full/10.1111/jmi.12765) se propose de partager en open-source un mécanisme de mise au point pour la microscopie frugale à lentille sphérique  imprimable en 3D. Ceci est motivé par le fait que lorsque l'on veut faire la mise au point en z au _Foldscope_, on se retrouve vite confronté à des limitations en précision qui trouvent leurs sources dans le fait que l'ajustement se fait à la main et n'est donc pas très fin.

Cet état de l'art des améliorations ergonomiques du _Foldscope_ n'est malheuresument pas très étoffé car rares sont les articles qui décrivent en détail la façon qu'ils ont eu d'utiliser ce microscope et rares sont ceux qui se sont penchés sur l'ergonomicité de ce dernier.

C'est ainsi que j'ai donc tenté de créer de mon côté un socle dans lequel se trouverait le _Foldscope_ de telle sorte que l'on puisse l'utiliser de manière assez stable avec un téléphone. Dans cette approche, on aimerait que le téléphone (ou la caméra) reste fixe quitte à faire bouger l'échantillon à la place. On voit d'ailleurs après une brève utilisation sur un premier prototype qu'en fixant la lentille (et donc par conséquent la caméra), on se retrouve à devoir manier d'autres extrémités du _Foldscope_ pour déplacer l'échantillon (qui n'étaient pas prévues pour à la base) mais cela ne pose aucun soucis techniques. Nous y renviendrons plus tard dans un tutoriel expliquant l'utilisation pratique du socle.

Le challenge ici a été de trouver des points sur le _Foldscope_ facilement "fixable" au socle. J'ai rapidement su identifier deux parties du microscope qui pouvaient satsisfaire à ces conditions (ces dernières devant absolument appartenir au "bloc" comportant le dispositif optique car nous voulons avant tout fixer l'optique du microscope). En analysant de près le _Foldscope_ ainsi que sa cinématique, on se rend vite compte que deux zones en particulier attirent notre attention.

La première est la zone qui permet au _Foldscope_ de s'ouvrir ainsi que de se rabattre sur lui-même. Lorsque le _Foldscope_ est "fermé", cette zone crée une sorte de boucle dans laquelle on pourrait insérer une "tige" et fixer cette dernière pour immobiliser en partie notre appareil. Ceci est représenté ci-dessous : 

![fix2](img/fix2.png)
![fix1](img/fix1.png)

La deuxième zone est un trou qui se crée lorsque l'on baisse l'échantillon. On peut imaginer fixer cette zone du _Foldscope_ de pleins de manières différentes. Faire passer une "tige" (qui serait solidaire au socle) au travers de ce trou est une option par exemple.

![fix3](img/fix3.png)

Voici ce à quoi ressemble le prototype en carton du socle en question :

![socle1](img/foldscope1.jpg)
![socle2](img/foldscope2.jpg)
![socle3](img/foldscope3.jpg)

Il est maintenant possible de fixer la caméra d'un téléphone à l'aide d'aimants à l'optique grâce aux coupleurs magnétiques fournis avec le _Foldscope_. Ainsi, l'ensemble caméra/optique serait rigide et l'observation pourrait se faire dans les meilleures conditions possibles. 
Seul problème, la caméra d'un iPhone (téléphone que je possède) n'est pas réellement compatible avec les coupleurs magnétiques car ceux-ci font vibrer les moteurs au niveau de l'optique du téléphone. Il était donc inenvisageable pour moi d'utiliser le socle de cette façon là et j'ai donc envisagé une autre piste pour fixer la caméra. 

L'idée serait de fixer la position de la caméra du téléphone à l'aide d'une "perche" à téléphone (voir photo ci-dessous) afin de centrer celle-ci sur l'axe optique de la lentille. Il s'avère que cette option est à jeter car elle rend l'utilisation du _Foldscope_ très contraignante et non-ergonomique. Le problème principal vient du fait qu'en décorrelant la lentille de la caméra, de nouveaux degrés de liberté apparaissent de sorte que l'alignement de la caméra sur l'axe optique ne devienne trop compliqué à réaliser car notre système devient trop sensible aux petites perturbations (le carton se plie lorsque l'on manipule le _Foldscope_, la "perche" n'est pas aussi stable qu'elle ne laisse penser, etc.). 

![socle4](img/foldscope4.jpg)

Nous pourrions améliorer le prototype en le fabriquant à l'aide d'une imprimante 3D pour renforcer la rigidité du socle ainsi que sa stabilité. Malheureusement, je ne possède pas les compétences nécessaires à la conception d'un tel objet mais c'est tout de même un chemin à approfondir. 


### 17 Octobre 2022 : Conclusion et transmission

Je vais maintenant m'atteler à la conception finale du socle à _Foldscope_. L'utilisation du carton est à mettre de côté car ce matériau n'est pas assez durable dans le temps et a tendance à se plier trop rapidement. Aussi, comme dit précédemment, le maniement d'une imprimante 3D ne fait pas partie de mes compétences donc c'est aussi une piste à mettre de côté. 

J'ai donc été "contraint" de travailler le bois pour mener à bien ce projet. Plus précisément, j'ai travillé avec des chutes de MDF (medium-density fibreboard), un matériau constitué de sciures de bois compactées afin d'obtenir des plaques assez denses et homogènes. Ces matériaux sont trouvables dans n'importe quel magasin de bricolage. Le choix de travailler le "bois" était en fait assez naturel pour moi car je disposais de tous les outils nécessaires à la réalisation de presque n'importe quel projet. 

Le prototype en carton en tant que tel était très satisfaisant mais manquait de rigidité car offrait trop de souplesse lorsque l'on bougeait l'échantillon avec les différentes "bandelettes" amovibles du _Foldscope_. Ces problèmes devraient disparaitre avec l'utilisation de MDF, beaucoup moins souple aux contraintes.

Un deuxième problème qui apparaissait était la non-compatibilité du coupleur magnétique avec la caméra de mon téléphone. Afin d'y remédier, j'ai décidé d'attacher le coupleur magnétique à l'objectif à l'aide de _patafix_. Celle-ci agit à la fois comme "tampon" entre le coupleur magnétique et l'objectif de mon téléphone (de sorte que l'aimant ne pertubre pas trop la caméra) mais aussi comme un moyen de fixer ces derniers. L'utilisation de _patafix_ est donc une solution en or car elle règle deux problèmes en un coup. 

![patafixlens](img/patafixlens.jpg)

L'utilisation de _patafix_ s'est avérée être très concluante car il est maintenant possible de prendre des photos avec mon téléphone sans aucun problème. Comme dit plus haut nous avons réglé deux problèmes majeurs en intégrant la _patafix_ dans le dispositif :

- l'alignement est désormais garanti, là où ça n'allait pas forcément être le cas avec la "perche" envisagée plus haut.
- le socle est maintenant compatible avec n'importe quel téléphone car le coupleur peut-être accroché à tout appareil à l'aide de _patafix_.

Afin de réellement construire le socle à partir de planches de MDF, il m'a fallu quelques outils pour manier le bois et faire les finitions que voici :

![tools](img/tools.jpg)


Ces outils sont, dans l'ordre :

- une équerre,
- du papier de verre, 
- de la colle à bois, 
- un ciseau à bois, 
- un maillet, 
- une lime.

J'ai aussi eu besoin d'outils nécessaires à la découpe du bois :

![toolsbis](img/toolsbis.jpg)

On a ici, toujours dans l'ordre :

- une scie à bois, 
- une serre-joint, 
- une défonceuse.

J'ai commencé par faire un trou rectangulaire de 5,9cm x 7cm dans une chute de MDF. Ce trou servira à y insérer la partie avec le "clapet" de l'arrière du _Foldscope_ (coté jaune). Pour ce faire j'ai utilisé une défonceuse (qui est un outil qui sert à perforer des planches en bois). J'ai tout de même demandé l'aide de mon père au début car le maniement de cet outils demande une certaine expertise. 

![defonceuse](img/defonceuse.jpg)

J'ai alors fini le découpage au ciseau à bois car le travail à la défonceuse n'a pas été très précis (ou les mesures ne l'étaient pas) et le trou créé était trop petit pour y insérer le _Foldscope_.

![scisor](img/scisor.jpg)


Ensuite, j'ai scié une surface de 13cm x 20,7cm de la grande chute (contenant le trou) afin d'avoir la base de mon socle où je poserais plus tard le _Foldscope_. 

![sawing](img/sawing.jpg)

La découpe à la scie est en fait plus technique qu'elle ne laisse penser. En effet, pour assurer une découpe droite dans une planche en bois, il est indispensable d'aligner l'arrête de la planche à son reflet dans la scie (voir photo ci-dessous).

![saw](img/saw.jpg)

Ensuite, il restait à surélever le socle à l'aide MDF. Tout a été fixé à la colle à bois. Cette partie n'était pas trop technique et a été assez rapide. Par contre, le dispositif permettant de fixer le _Foldscope_ au socle, lui, a requis un peu plus de réflexion. 

Je suis alors parti sur un mécanisme où on insérerait le "clapet" du microscope dans une fine fente à l'extrémité basse du trou déjà préformé et on le replierait de telle sorte que le _Foldscope_ reste en place sur sa partie inférieure. Une photo de la situation et représentée ci-dessous.

![slide1](img/slide1.jpg)
![slide2](img/slide2.jpg)

Afin de fixer la partie supérieur du _Foldscope_, j'ai placé un "bouton" en forme de trapèze inversé juste au dessus du trou préformé. Il est intéressant de noter que le plan initial était de mettre un bouton parallélépipèdique mais je me suis rendu compte qu'en travaillant le bois et en coupant des bouts de MDF au ciseau à bois, je créais des chutes qui présentaient des faces en biais (Ceci est en fait du à la morphologie d'un ciseau à bois). En répétant ceci sur l'autre face de la chute de MDF, il m'a été très facile de créer ce bouton en forme de trapèze inversé. Cette forme est bien mieux qu'un parallélépipède car elle permet de contraindre le _Foldscope_ à rester "à plat" sur le socle. Ce bouton s'insère aisément dans le  trou qui apparait en haut du _Foldscope_ lorsqu'on baisse l'échantillon (toujours aucune modification n'a été effectué sur le microscope jusqu'ici).

![button](img/button.jpg)

Voici ce que donne le rendu final de l'armature du socle.

![final](img/finalrendu.jpg)

Ensuite, dans une optique d'optimisation du socle afin de lui apporter une ergonomie plus poussée, j'ai commencé à sérieusement envisager l'idée d'insérer une LED fixe au socle car jusqu'ici, la question de l'éclairage n'avait toujours pas été posée. Pour ce faire, j'ai du trouver un moyen d'alimenter la LED. Deux options s'offraient alors à moi : utiliser des piles ou une alimentation "externe" à l'aide d'un câble. J'ai décidé de partir sur la deuxième option car pour intégrer des piles au dispositif, il aurait fallu trouver une façon de souder le boitier dans lequel se trouvent les piles au socle. De plus, l'alimentation par câble ne souffre pas du fait qu'il faille parfois être capable de recharger les piles.

J'ai ainsi démarré l'assemblage du "circuit éléctrique" qui conntiendrait la LED. J'ai donc eu besoin d'une LED blanche de 3.3 V, d'une résistance de 47 Ohm et d'un câble USB. En soudant la résistance à la LED et en insérant les deux branches de la LED dans l'autre extremité du cable USB, on obtient un dispositif qui lorsqu'il est branché à une source de courant, illumine la LED.

![LED](img/led.png)
![cable](img/cable.png)

Voici ce que cela donne lorsque le tout est branché à un ordinateur. 

![litled](img/litled.png)

Afin d'insérer le câble à l'intérieur du socle, il a fallu créer un "trou de souris" sur la bas du socle à l'aide d'une fraiseuse pour pouvoir le faire passer. 

![fraise](img/fraise.png)
![micehole](img/micehole.png)

Ensuite, il a suffit de fixer la LED de sorte qu'elle soit bien centrée sur l'axe optique. C'est ce que j'ai fait en utilisant, une fois de plus, de la _patafix_ pour coller le rectangle en plastique noir sous la LED au socle. 

![fixedled](img/fixedled.png)

Voici ce que donne le rendu final avec la LED et le _Foldscope_.

![finalled](img/finalled.png)

Ce dispositif, dont [voici un tutoriel](https://youtu.be/bSNKFV8Lpc0) se révèle faciliter grandement l'utilisation du _Foldscope_. Le défi que je m'étais lancé est donc relevé dans le sens où l'ergonomicité du microscope a été améliorée et que ces améliorations sont accessibles à tout un chacun possédant les outils nécessaires (mentionnés plus haut). En effet, le but de cette section était avant tout de pouvoir partager mon expérience utilisateur du microscope amélioré mais aussi de permettre aux personnes interessées de reproduire le socle de leur côté en leur apportant un guide qui reprendrait pas-à-pas les étapes à réaliser pour y parvenir. J'espère avant tout avoir été le plus complet et exhaustif possible dans cette tâche afin que ma maitrise nouvellement acquise de l'utilisation du _Foldscope_ dans des conditions ergonomiques sois la plus abordable et accessible possible.  




